import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-test',
  template: `

  <h2>{{name | lowercase}}</h2>
  <h2>{{name | uppercase}}</h2>
  <h2>{{message | titlecase}}</h2>
  <h2>{{name | slice : 3}}</h2>
  <h2>{{name | slice : 3 : 6}}</h2>
  <h2>{{data | json}}</h2>
  
  <h2>{{5.768 | number :'1.2-3'}}</h2>
  <h2>{{5.768 | number :'3.4-5'}}</h2>
  <h2>{{5.768 | number :'3.1-2'}}</h2>
  <h2>{{5.768 | number :'1.2-3'}}</h2>

  <h2>{{0.45 | percent}}</h2>
  <h2>{{0.45 | currency}}</h2>
  <h2>{{0.45 | currency : 'INR'}}</h2>
  <h2>{{0.45 | currency : 'INR' : 'code'}}</h2>

  <h2>{{date}}</h2>
  <h2>{{date | date : 'short'}}</h2>
  <h2>{{date | date : 'shortDate'}}</h2>
  <h2>{{date | date : 'shortTime'}}</h2>

  <h2> MEDIUM </h2>
  <h2>{{date | date : 'medium'}}</h2>
  <h2>{{date | date : 'mediumDate'}}</h2>
  <h2>{{date | date : 'mediumTime'}}</h2>

  <h2>{{date | date : 'long'}}</h2>
  <h2>{{date | date : 'longDate'}}</h2>
  <h2>{{date | date : 'longTime'}}</h2>
  


  `,
  styles: []
})
export class TestComponent implements OnInit {

  public name = "RohanSomnathSanap"
  public message = "Welcome to the angular course"
  public data = {
    fName : "Rohan",
    mName : "Somnath",
    lName : "Sanap",
  }
  public date = new Date();
  constructor() { }

  ngOnInit(): void {
  }
  

}

// <h2>Hello {{name}}</h2>
//   <button (click)= "fireEvent()">Send Message</button>

//   @Input('parentData') public  name : any;
//   @Output() public childEvent = new EventEmitter;
//   fireEvent() {
//     this.childEvent.emit('Hey Rohan Its message from child....!')
//   }


// <h2 *ngIf= "displayName; else elseBlock" >Rohan Sanap</h2>
// <ng-template #elseBlock>
// <h2> Else block is executed </h2>
// </ng-template>

// <div *ngIf = "displayName; then thenBlock; else elseBlock"></div>
// <ng-template then, #thenBlock>
// <h2>Inside ng-template if block</h2>
// </ng-template>

// <ng-template else, #elseBlock>
// <h2>Inside ng-template else block</h2>
// </ng-template>

// <div [ngSwitch] = "color">
//   <div *ngSwitchCase="'red'">You Picked up Red color</div>
//   <div *ngSwitchCase="'blue'">You Picked up Blue color</div>
//   <div *ngSwitchCase="'green'">You Picked up Green color</div>
//   <div *ngSwitchDefault> Pick again</div>
// </div>

// <div *ngFor="let color of colors; index as i; odd as o; first as f; last as l; even as e" >
// <h2>Index color isOdd isEven isFirst  isLast</h2>
// <h2>{{i}} {{color}} {{o}} {{e}} {{e}} {{f}} {{l}}</h2>
// </div>

// public displayName = true
//   public color ="red"
//   public colors =["red","green","black","yellow","blue"]
  